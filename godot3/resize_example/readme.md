Para que funcione el redimensionado, teniendo en el centro la pantalla de juego y tener un marco (en plan como en los emuladores):
 - hay que crear una escena de tipo control (no nodo2d)
 - cambiar el anchor (la X verde) a que ocupe todo el viewport
 - Añadir un color rect, y en el menú de arriba de Layout ponerlo centrado

Mientras en:
 - En otra escena crear el juego tal cual sin marco.

Volver a la escena principal de control:
 - Añadir la escena del juego como hija del "color rect" y centrarla si es necesario
 - Añadir también como hijo al "color rect" la imagen que se va usar como marco del juego

En las propiedades del proyecto o project settings hay que cambiar varias cosas:
 - Display -> Window -> Stretch
   - Mode 2d (no se que en que influye, viewport también funciona).
   - Aspect: **expand** obligatorio este porque hace que la ventana si se encoje encoja el juego y si se amplia tambien
     - pero si cambia el aspect ratio, se hace una ventana alargada a lo ancho o a lo alto, no mueve la pantalla del juego de enmedio y muestra el marco lo que no se vea...aunque claro tiene el limite del tamaño de la imagen...y se vera gris cuando termine...o negro.
 - Rendering -> Enviroment
   - Default Clear Color: aquí es donde se pone el color de fondo cuando amplia tanto que no hay dibujo del marco, ponerlo en negro queda bien.
