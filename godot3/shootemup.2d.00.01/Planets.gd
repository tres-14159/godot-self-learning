extends Node

var min_speed = 10
var max_speed = 400

var speeds := []
var acelerations := []
var rng = RandomNumberGenerator.new()

var planets
var timer

func _timeout():
	changeAcelerations(false)

func changeAcelerations(first):
	var i = 0
	for planet in planets:
		var current_speed = speeds[i]
		var aceleration = rng.randi_range(-current_speed, max_speed - current_speed)
		if (first):
			acelerations.append(aceleration)
		else:
			acelerations[i] = aceleration
		i += 1

func _ready():
	planets = get_tree().get_nodes_in_group("planets")
	for planet in planets:
		speeds.append(min_speed)
	changeAcelerations(true)
	timer = Timer.new()
	timer.autostart = true
	timer.wait_time = 1
	timer.one_shot = false
	timer.connect("timeout", self, "_timeout")
	add_child(timer)

func _process(delta):
	var i = 0
	for planet in planets:
		var current_speed = speeds[i]
		current_speed += acelerations[i] * delta
		speeds[i] = current_speed
		planet.position.x -= current_speed * delta
		i += 1
