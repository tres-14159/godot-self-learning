extends Area2D

export var acceleration := 400
var decelaration := acceleration / 4
var current_speed := Vector2(0, 0)

# Is the same
# var current_speed := Vector2.ZERO

var time_pressed := 0

var game_over = preload("res://gameover.tscn").instance()

func _process(delta):
	var pressed = false
	
	# vector normalizado 0 - 1, si es joystick hay 0.42 si es tecla 0 a 1
	var input_vector = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	if input_vector.length() != 0:
		pressed = true
		current_speed += input_vector * acceleration * delta
	else:
		for i in 2:
			if current_speed[i] != 0:
				if current_speed[i] < 0:
					current_speed[i] += decelaration * delta
					current_speed[i] = 0 if current_speed[i] > 0 else current_speed[i]
				else:
					current_speed[i] -= decelaration * delta
					current_speed[i] = 0 if current_speed[i] < 0 else current_speed[i]
	
	position += current_speed * delta

func shoot_laser():
	var shoot: Area2D = preload("res://ship/shoot/Shoot.tscn").instance()
	
	get_viewport().add_child(shoot)
	shoot.global_position = $SalidaDisparo.global_position
	shoot.global_scale = $SalidaDisparo.global_scale * Vector2(5, 5)

func _unhandled_input(event):
	if event.is_action_pressed("shoot"):
		if $repeticion_disparo.is_stopped():
			shoot_laser()
			$repeticion_disparo.start()
	if event.is_action_released("shoot"):
		$repeticion_disparo.stop()

func _on_Area2D_area_entered(area):
#	return
	if area:
		get_tree().root.add_child(game_over)


func _on_Timer_timeout():
	shoot_laser()
