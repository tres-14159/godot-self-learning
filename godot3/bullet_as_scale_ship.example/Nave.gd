extends Sprite

export var max_speed := 200.0

func _process(delta):
	if Input.is_action_pressed("down"):
		position.y += max_speed * delta
	if Input.is_action_pressed("up"):
		position.y -= max_speed * delta
	if Input.is_action_pressed("left"):
		position.x -= max_speed * delta
	if Input.is_action_pressed("right"):
		position.x += max_speed * delta

func _unhandled_input(event):
	if event.is_action_pressed("shoot"):
		var disparo := preload("res://disparo.tscn").instance()
		get_tree().get_root().add_child(disparo)
		disparo.global_position = $SalidaDisparo.global_position
		disparo.global_scale = $SalidaDisparo.global_scale * Vector2(5, 5)
