extends Sprite

export var max_speed := 100.0

func _process(delta):
	position.x += delta * max_speed
