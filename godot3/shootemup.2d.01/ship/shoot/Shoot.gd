extends Area2D

export var speed := 100.0

var direction_ship := Vector2.RIGHT

# Called when the node enters the scene tree for the first time.
func _ready():
	rotation = direction_ship.angle()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position += direction_ship * speed * delta


func _on_area_entered(object):
	if object.has_method("get_damage"):
		object.get_damage()
		queue_free()
 
