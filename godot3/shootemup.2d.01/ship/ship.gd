extends Area2D

var rotation_speed := 5.0
var acceleration := 1000.0
var max_speed := 500.0
var velocity := Vector2.ZERO

export var angular_speed := 0.0
var angular_aceleration := PI
var max_angular_speed := 2.0 * PI

func _process(delta):
#	Equivalent to Input.get_axis
#	var l := Input.get_action_strength("move_left")
#	var r := Input.get_action_strength("move_right")
#	var dir := r - l
	
	var dir := Input.get_axis("move_left", "move_right")
	var thrust := Input.get_action_strength("move_up")
	
	angular_speed = lerp(angular_speed, dir * max_angular_speed, angular_aceleration * delta)
	
	rotation += angular_speed * delta
	#velocity += Vector2.UP.rotated(rotation) * thrust * acceleration * delta
	# Esta es la manera mas optima pero es mas compleja de entender es la matriz transformación de este frame
	velocity += -transform.y * thrust * acceleration * delta
	velocity = velocity.clamped(max_speed)
	position += velocity * delta

func shoot_laser():
	var shoot: Area2D = preload("res://ship/shoot/Shoot.tscn").instance()
	shoot.direction_ship = Vector2.UP.rotated(rotation)
	get_viewport().add_child(shoot)
	
	shoot.global_position = $SalidaDisparo.global_position
	shoot.global_scale = $SalidaDisparo.global_scale * Vector2(5, 5)

func _unhandled_input(event):
	if event.is_action_pressed("shoot"):
		if $RepeticionDisparo.is_stopped():
			shoot_laser()
			$RepeticionDisparo.start()
	if event.is_action_released("shoot"):
		$RepeticionDisparo.stop()

func _on_Timer_timeout():
	shoot_laser()
	
func get_damage():
	queue_free()
