extends Path2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_asteroid():
	$PathFollow2D.unit_offset = rand_range(0, 1)
	var asteoid : Area2D = preload("res://asteroid/Asteroid.tscn").instance()
	asteoid.position = $PathFollow2D.position
	# Haciendo el aleatorio la velocidad desde aqui
	#asteoid.speed = $PathFollow2D.transform.y * rand_range(10, 100)
	asteoid.speed = asteoid.speed.rotated($PathFollow2D.rotation + (PI / 2))
	add_child(asteoid)
