extends Area2D

export var rotate_speed := 0.0
export var speed := Vector2.ZERO
export var resize := 0.0
var init_position := Vector2.ZERO


# Called when the node enters the scene tree for the first time.
func _init():
	speed = Vector2(rand_range(10, 200), 0)
	rotate_speed = rand_range(-PI, PI)
	scale *= rand_range(0.1, 1.0)

func _ready():
	init_position = position

func _process(delta):
	rotation += rotate_speed * delta
	position += speed * delta
	if (position - init_position).length() > 1500: # Borramos a lo bruto los que se salen asi
		queue_free()
		
func get_damage():
		queue_free()


func _on_area_entered(object):
	if object.has_method("get_damage"):
		object.get_damage()
