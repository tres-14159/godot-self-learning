extends Sprite

var acceleration := 100
var decelaration := acceleration / 4
var current_speed := Vector2(0, 0)
# Is the same
# var current_speed := Vector2.ZERO

var time_pressed := 0

var game_over = preload("res://gameover.tscn").instance()

func _process(delta):
	var pressed = false
	
	# vector normalizado 0 - 1, si es joystick hay 0.42 si es tecla 0 a 1
	var input_vector = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	if input_vector.length() != 0:
		pressed = true
		current_speed += input_vector * acceleration * delta
	else:
		for i in 2:
			if current_speed[i] != 0:
				if current_speed[i] < 0:
					current_speed[i] += decelaration * delta
					current_speed[i] = 0 if current_speed[i] > 0 else current_speed[i]
				else:
					current_speed[i] -= decelaration * delta
					current_speed[i] = 0 if current_speed[i] < 0 else current_speed[i]
	
	position += current_speed * delta

func _on_Area2D_area_entered(area):
#	return
	if area:
		get_tree().root.add_child(game_over)
